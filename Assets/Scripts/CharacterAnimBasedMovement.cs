using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(CharacterController))]
[RequireComponent(typeof(Animator))]
public class CharacterAnimBasedMovement : MonoBehaviour
{

    public float rotationSpeed = 4f;
    public float rotationThreshold = 0.3f;

    [Range(0, 180)]
    public float degreesToTurn = 180f;

    [Header("Animator Parameters")]
    public string motiomParam = "motion";
    public string mirroridleParam = "mirrorIdle";
    public string turn180Param = "turn180";

    [Header("Animator Smoothing")]
    [Range(0, 1f)]
    public float startAnimTime = 0.3f;
    [Range(0, 1f)]
    public float stopAnimTime = 0.15f;

    private Ray wallRay = new Ray();
    private float speed;
    private Vector3 desiredMoveDirection;
    private CharacterController caracterCrontroller;
    private Animator animator;
    private bool mirrorIdle;
    private bool turn180;
    private Rigidbody Player;
    private float VelocidadSalto;
    private bool saltoFinal;

    public string salto = "Jump";

    // Start is called before the first frame update
    void Start()
    {
        caracterCrontroller = GetComponent<CharacterController>();
        animator = GetComponent<Animator>();
    }

    public void MoveCharacter(float hInput, float vInput, Camera cam, bool jump, bool dash)
    {
        speed = new Vector2(hInput, vInput).normalized.sqrMagnitude;

        if (speed >= speed - rotationThreshold && dash)
        {
            speed = 1.5f;
        }

        if (speed > rotationThreshold)
        {
            animator.SetFloat(motiomParam, speed, startAnimTime, Time.deltaTime);
            Vector3 foward = cam.transform.forward;
            Vector3 right = cam.transform.right;

            foward.y = 0f;
            right.y = 0f;

            foward.Normalize();
            right.Normalize();

            desiredMoveDirection = foward * vInput + right * hInput;

            if (Vector3.Angle(transform.forward, desiredMoveDirection) >= degreesToTurn)
            {
                turn180 = true;
            }
            else
            {
                turn180 = false;
                transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(desiredMoveDirection), rotationSpeed * Time.deltaTime);
            }
            animator.SetBool(turn180Param, turn180);
            animator.SetFloat(motiomParam, speed, stopAnimTime, Time.deltaTime);
        }
        else if (speed < rotationThreshold)
        {
            animator.SetBool(mirroridleParam, mirrorIdle);

            animator.SetFloat(motiomParam, speed, stopAnimTime, Time.deltaTime);
        }

        if (jump)
        {
            saltoFinal = true;

            animator.SetBool(salto, saltoFinal);
        }
        else
        {
            saltoFinal = false;
            animator.SetBool(salto, saltoFinal);
        }

    }

    private void OnAnimatorIK(int layerIndex)
    {
        if (speed < rotationThreshold)
        {
            return;
        }

        float distanceToFootLeft = Vector3.Distance(transform.position, animator.GetIKPosition(AvatarIKGoal.LeftFoot));
        float distanceToFootRight = Vector3.Distance(transform.position, animator.GetIKPosition(AvatarIKGoal.RightFoot));

        if (distanceToFootRight < distanceToFootLeft)
        {
            mirrorIdle = true;
        }
        else
        {
            mirrorIdle = false;
        }

    }

}